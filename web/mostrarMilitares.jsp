<%-- 
    Document   : mostrarMilitares
    Created on : 03/11/2019, 01:26:58
    Author     : Leandro
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Muestro militares</title>
    </head>
    <body>

        <c:choose>
            <c:when test="${empty militares}">
                <h1>ERROR</h1>
                <p>
                    No existen militares cargados.
                </p>
            </c:when>
            <c:otherwise>
                <h1>
                    Lista de militares
                </h1>
                <table style="margin: 0 auto;">

                    <c:forEach var="militar" items="${militares}" >            

                        <tr>
                            <th> Tipo: ${militar.tipo} </th>
                            <th>Codigo: ${militar.codigo} </th>
                            <th>Nombre: ${militar.nombre} </th>
                            <th>Apellido: ${militar.apellido} </th>
                            <th>Graduacion: ${militar.graduacion} </th>
                        </tr>    
                        <c:if test="${(militar.tipo == 'Soldado')}">
                            
                            <%--
                           Compania: ${companias[militar.codCompania].actividad}&nbsp;&nbsp;
                           Cuerpo: ${cuerpos[militar.codCuerpo].denominacion}&nbsp;&nbsp;
                           Cuartel: ${cuarteles[militar.codCuartel].nombre}&nbsp;&nbsp;
                           
                            Compania: ${companias[55]}&nbsp;&nbsp;
                           Compania: ${companias['55']}&nbsp;&nbsp;
                           Compania: ${companias[55].value}&nbsp;&nbsp;
                           Compania: ${companias['55'].value}&nbsp;&nbsp;
                            --%>
                            <tr>
                                <th></th>
                                <th>Compania:

                                    <c:forEach var="compania" items="${companias}" >
                                        <c:if test="${(militar.codCompania == compania.codigo)}">
                                            ${compania.actividad}
                                        </c:if>
                                    </c:forEach></th>
                                <th>
                                    Cuerpo:
                                    <c:forEach var="cuerpo" items="${cuerpos}" >
                                        <c:if test="${(militar.codCuerpo == cuerpo.codigo)}">
                                            ${cuerpo.denominacion}
                                        </c:if>
                                    </c:forEach></th>
                                <th>
                                    Cuartel:
                                    <c:forEach var="cuartel" items="${cuarteles}" >
                                        <c:if test="${(militar.codCuartel == cuartel.codigo)}">
                                            ${cuartel.nombre}
                                        </c:if>
                                    </c:forEach></th>                            
                            </tr>
                            <tr></tr><tr></tr>
                            <tr>
                                <th></th><th></th>
                                <th>Servicios Asignados:</th>
                                <th> </th>
                            </tr>
                            <c:forEach var="servicioRealizado" items="${serviciosRealizados}" >

                                <c:if test="${(militar.codigo == servicioRealizado.codSoldado)}">
                                    <tr>
                                        <th></th>
                                            <c:forEach var="servicio" items="${servicios}" >
                                                <c:if test="${servicio.codigo == servicioRealizado.codServicio}">
                                                <th>  Descripcion: ${servicio.descripcion}</th>
                                                <th></th>   
                                            </c:if>
                                            </c:forEach>
                                        <th>Fecha: ${servicioRealizado.fecha}</th>

                                    </tr>

                                </c:if>

                            </c:forEach>
                            <tr></tr><tr></tr>
                        </c:if>
                            <tr></tr><tr></tr>   
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>

    </body>
</html>
