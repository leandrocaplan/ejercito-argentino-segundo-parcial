<%-- 
    Document   : bajaMilitar
    Created on : 03/11/2019, 04:28:55
    Author     : Leandro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Desasignar servicio: </title>
    </head>
    <body>
        <h1>
        Seleccione el servicio a desasignar:
        </h1>

        <form method="post" action="ControladorDesasignarServicio">
            <select name="servicioDesasignado" size="1" style="width:900px; height:40px" >
                <c:forEach items="${serviciosRealizados}" var="servicioRealizado" >
                    <option value="${servicioRealizado.codServicioRealizado}" align="center"> 

                        <c:forEach var="servicio" items="${servicios}" >
                            <c:if test="${servicio.codigo == servicioRealizado.codServicio}">
                                Codigo de servicio: ${servicio.codigo}&nbsp;&nbsp;
                                Descripcion: ${servicio.descripcion}&nbsp;&nbsp;
                            </c:if>
                        </c:forEach>    
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <c:forEach var="militar" items="${militares}" >
 
                            <c:if test="${militar.codigo == servicioRealizado.codSoldado}">
                                
                                Codigo de soldado: ${militar.codigo}&nbsp;&nbsp;
                                Nombre: ${militar.nombre}&nbsp;&nbsp;
                                Apellido: ${militar.apellido}&nbsp;&nbsp;
                            </c:if>
                        </c:forEach> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                       Fecha: ${servicioRealizado.fecha}         
                    </option>
                </c:forEach>
            </select>
            <br>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
            
            <input type="submit" style="width:60px; height:40px" value ="Enviar">
        </form>
    </body>
</html>
