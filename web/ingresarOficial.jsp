<%-- 
    Document   : ingresarMilitar
    Created on : 02/11/2019, 03:52:51
    Author     : Leandro
--%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Ingresar o modificar oficial</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del oficial:
        </h1>
        <h2>
            Si existe un oficial con el c�digo ingresado, se modificar�.
            <br>
            Si no existe un oficial con el c�digo ingresado, se agregar�.

        </h2>

        <form method="post" action="ControladorIngresarOficial">
            <p>
            <table style="margin: 0 auto;">
                <tr>
                    <th>Codigo:</th>
                    <th> <input type="number" name="codigo"></th>
                </tr>
                <tr>
                    <th>Contrase�a:</th>
                    <th><input type="password" name="password"></th>
                </tr>
                <tr> 
                    <th>Nombre:</th>
                    <th> <input type="text" name="nombre"></th>
                </tr>
                <tr>
                    <th>Apellido:</th>
                    <th><input type="text" name="apellido"></th>
                </tr>
                <tr>
                    <th> Graduaci�n: </th>

                    <th>
                        <select name="graduacion" size="1">
                            <option value="Teniente general"> Teniente general </option>
                            <option value="General de divisi�n"> General de divisi�n </option>
                            <option value="General de brigada"> General de brigada </option>
                            <option value="Coronel mayor"> Coronel mayor </option>
                            <option value="Coronel"> Coronel </option>
                            <option value="Teniente coronel"> Teniente coronel </option>
                            <option value="Mayor"> Mayor </option>
                            <option value="Capitan"> Capitan </option>
                            <option value="Teniente primero"> Teniente primero </option>
                            <option value="Subteniente"> Subteniente </option>

                        </select>
                    </th>
                    </table>
                <input type="hidden" name="dirIP" value="${dirIP}">
                <input type="hidden" name="nomBD" value="${nomBD}">
                <br>
                   <input type="submit" style="width:110px; height:40px" value ="Enviar">  
                 
            
        </p>
    </form> 
</body>
</html>