<%-- 
    Document   : ingresarCuartel.jsp
    Created on : 02/11/2019, 19:33:29
    Author     : Leandro
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Ingresar o modificar cuartel</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del cuartel:
        </h1>
        <h2>
            Si existe un cuartel con el c�digo ingresado, se modificar�.
            <br>
            Si no existe un cuartel con el c�digo ingresado, se agregar�.

        </h2>
        <form method="post" action="ControladorIngresarCuartel">

            <table style="margin: 0 auto;">
                <tr>
                    <th>Codigo:</th>
                    <th> <input type="number" name="codigo"> </th>
                </tr>
                <tr>
                    <th>Nombre:</th>
                    <th><input type="text" name="nombre"></th>
                </tr>
                <tr>
                    <th>Ubicacion:</th>
                    <th> <input type="text" name="ubicacion"></th>
                </tr>
                
            </table>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  

        </form> 
    </body>
</html>
