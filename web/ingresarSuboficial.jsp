<%-- 
    Document   : ingresarMilitar
    Created on : 02/11/2019, 03:52:51
    Author     : Leandro
--%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Ingresar o modificar suboficial</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del suboficial:
        </h1>
        <h2>
            Si existe un suboficial con el c�digo ingresado, se modificar�.
            <br>
            Si no existe un suboficial con el c�digo ingresado, se agregar�.

        </h2>

        <form method="post" action="ControladorIngresarSuboficial">
            <p>
            <table style="margin: 0 auto;">
                <tr>
                    <th>Codigo:</th>
                <th><input type="number" name="codigo"></th>
                </tr>
                <tr>
                    <th>Contrase�a:
                <th><input type="password" name="password"></th>
                </tr>
                <tr>
                    <th>Nombre:
                <th><input type="text" name="nombre"></th>
                </tr>
                <tr>
                    <th>Apellido:
                <th><input type="text" name="apellido"></th>
                </tr>
                <tr>
                    <th>Graduaci�n:</th>
                    <th>
                <select name="graduacion" size="1">

                    <option value="Suboficial Mayor"> Suboficial Mayor </option>
                    <option value="Suboficial Principal"> Suboficial Principal </option>
                    <option value="Sargento Ayudante"> Sargento Ayudante </option>
                    <option value="Sargento Primero"> Sargento Primero </option>
                    <option value="Sargento"> Sargento </option>
                    <option value="Cabo Primero"> Cabo Primero </option>
                    <option value="Cabo"> Cabo </option>


                </select></th>
                        
                </tr>
                <input type="hidden" name="dirIP" value="${dirIP}">
                <input type="hidden" name="nomBD" value="${nomBD}">
            </table>
            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  

            </p>
        </form> 
    </body>
</html>