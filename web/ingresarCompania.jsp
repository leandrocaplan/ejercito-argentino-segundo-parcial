<%-- 
    Document   : ingresarCompania
    Created on : 02/11/2019, 03:51:38
    Author     : Leandro
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Ingresar o modificar compa�ia</title>
    </head>
    <body>
        <h1>
            Ingrese los datos de la compa��a:
        </h1>
        <h2>
            Si existe una compa��a con el c�digo ingresado, se modificar�.
            <br>
            Si no existe una compa��a con el c�digo ingresado, se agregar�.

        </h2>
        <form method="post" action="ControladorIngresarCompania">
            <p>
            <table style="margin: 0 auto;">
                <tr>
                    <th>Codigo:</th>
                    <th><input type="number" name="codigo"></th>
                </tr>
                <tr>
                    <th> Actividad:</th>
                    <th><input type="text" name="actividad"></th>
                </tr>
            </table>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form> 
    </body>
</html>
</html>