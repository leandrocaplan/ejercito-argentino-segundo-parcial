<%-- 
    Document   : ingresarMilitar
    Created on : 02/11/2019, 03:52:51
    Author     : Leandro
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Asignar servicio a un soldado</title>
    </head>
    <body>
        <h1>
            Seleccione los datos del servicio a asignar:
        </h1>

        <form method="post" action="ControladorAsignarServicio">
            <table style="margin: 0 auto;">
                <tr>
                    <th>Soldado:</th>
                    <th>
                        <select name="codSoldado" size="1" >
                            <c:forEach items="${militares}" var="militar" >
                                <c:if test="${(militar.tipo == 'Soldado')}">
                                    <option value="${militar.codigo}" align="center"> 
                                        Codigo: ${militar.codigo} &nbsp;&nbsp;
                                        Nombre: ${militar.nombre} &nbsp;&nbsp;
                                        Apellido: ${militar.apellido} &nbsp;&nbsp;
                                    </option>
                                </c:if>
                            </c:forEach>
                        </select></th>
                </tr>
                <tr>
                    <th> Servicio:</th>
                    <th>
                <select name="codServicio" size="1"  >
                    <c:forEach var="servicio" items="${servicios}" >
                        <option value="${servicio.codigo}" align="center"> 
                            Codigo: ${servicio.codigo} &nbsp;&nbsp;
                            Descripcion: ${servicio.descripcion} &nbsp;&nbsp;

                        </option>
                    </c:forEach>       
                </select></th>
                </tr>
                <tr>
                    <th>Fecha:</th>
                    <th><input type="date" name="fecha"></th> 
                </tr>
            </table>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form> 

    </body>
</html>