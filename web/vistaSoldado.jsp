
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Ingreso al Sistema del Ejercito Argentino: </title>
    </head>
    <body>
                <h1>
                    ¡Bienvenido! Su ingreso fue exitoso
                </h1>
    
        <h2>
                        Ud ingreso como: ${graduacion} ${nombre} ${apellido} 
 
        </h2>           
        <form method="post" action="ControladorOpcionesMilitar">
            <p>
                Sus opciones son:
                <select name="opcion" size="1" style="width:220px; height:40px">
                    
                    <option value="mostrar_militares">Mostrar militares</option>
                    <option value="mostrar_companias">Mostrar companías</option>
                    <option value="mostrar_cuerpos">Mostrar cuerpos</option>
                    <option value="mostrar_cuarteles">Mostrar cuarteles</option>
                    <option value="mostrar_servicios">Mostrar servicios</option>
                    <option value="logout"> Logout </option>
                </select>

                <input type="hidden" name="dirIP" value="${dirIP}">
                <input type="hidden" name="nomBD" value="${nomBD}">
                <input type="submit" style="width:60px; height:40px" value ="Enviar">
            </p>
        </form>
             
            
    </body>
</html>