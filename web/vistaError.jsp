<%-- 
    Document   : vistaError.jsp
    Created on : 02/11/2019, 03:18:23
    Author     : Leandro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Error: </title>
    </head>
    <body>
        <h1>Hubo un error : ${mensajeError}</h1>
    </body>
</html>
