<%-- 
    Document   : mostrarMilitares
    Created on : 03/11/2019, 01:26:58
    Author     : Leandro
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Muestro cuerpos</title>
    </head>
    <body>


        <c:choose>
            <c:when test="${empty cuerpos}">
                <h1>ERROR</h1>
                <p>
                    No existen cuerpos cargados.
                </p>
            </c:when>
            <c:otherwise>
                <h1>
                    Lista de cuerpos:
                </h1>
                <table style="margin: 0 auto;">

                    <c:forEach var="cuerpo" items="${cuerpos}" >
                        <tr>
                            <th>Codigo: ${cuerpo.codigo} </th>
                            <th>Denominacion: ${cuerpo.denominacion}</th>
                        </tr>
                    </c:forEach>

                </table>
            </c:otherwise>
        </c:choose>

    </body>
</html>
