<%-- 
    Document   : ingresarCuerpo
    Created on : 02/11/2019, 03:51:57
    Author     : Leandro
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Ingresar o modificar cuerpo</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del cuerpo:
        </h1>
        <h2>
            Si existe un cuerpo con el c�digo ingresado, se modificar�.
            <br>
            Si no existe un cuerpo con el c�digo ingresado, se agregar�.

        </h2>
        <form method="post" action="ControladorIngresarCuerpo">
            <table style="margin: 0 auto;">
                <tr>
                    <th>Codigo:</th>
                    <th><input type="number" name="codigo"></th>
                </tr>
                <tr>
                    <th>Denominacion:</th>
                    <th> <input type="text" name="denominacion"></th>
                </tr>

            </table>        
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form> 
    </body>
</html>
</html>
