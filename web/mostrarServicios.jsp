<%-- 
    Document   : mostrarMilitares
    Created on : 03/11/2019, 01:26:58
    Author     : Leandro
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Muestro militares</title>
    </head>
        <body>

        <c:choose>
            <c:when test="${empty servicios}">
                <h1>ERROR</h1>
                <p>
                    No existen servicios cargados.
                </p>
            </c:when>
            <c:otherwise>
                <h1>
                    Lista de servicios:
                </h1>
                <table style="margin: 0 auto;">
                <c:forEach var="servicio" items="${servicios}" >
                    <tr>
                        <th> Codigo: ${servicio.codigo} </th>
                        <th> Descripcion: ${servicio.descripcion} </th>

                    <tr>
                </c:forEach>
            </c:otherwise>
        </c:choose>
                    
    </body>
</html>
