<%-- 
    Document   : bajaMilitar
    Created on : 03/11/2019, 04:28:55
    Author     : Leandro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Ingreso al Sistema del Ejercito Argentino: </title>
    </head>
    <body>
        <h1>
        Seleccione el militar a dar de baja:
        </h1>
        <form method="post" action="ControladorBajaMilitar">
            <select name="militarBaja" size="1" style="width:600px; height:40px" >
                <c:forEach items="${militares}" var="militar" >
                    <option value="${militar.codigo}" align="center"> 
                        Codigo: ${militar.codigo} &nbsp; , &nbsp; 
                        Nombre: ${militar.nombre} &nbsp; , &nbsp;
                        Apellido: ${militar.apellido}
                    </option>
                </c:forEach>
            </select>
            <br>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
            <br>
            <input type="submit" style="width:60px; height:40px" value ="Enviar">
        </form>
    </body>
</html>
