<%-- 
    Document   : bajaMilitar
    Created on : 03/11/2019, 04:28:55
    Author     : Leandro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Dar de baja una compañía </title>
    </head>
    <body>
        <h1>
        Seleccione la compañía a dar de baja:
        </h1>
        <form method="post" action="ControladorBajaCompania">
            <p>
            <select name="companiaBaja" size="1" style="width:600px; height:40px" >
                <c:forEach items="${companias}" var="compania" >
                    <option value="${compania.codigo}" align="center"> 
                        Codigo: ${compania.codigo} &nbsp; , &nbsp; 
                        Nombre: ${compania.actividad}

                    </option>
                </c:forEach>
            </select>
            <br>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
            <br>
            <input type="submit" style="width:60px; height:40px" value ="Enviar">
            </p>
        </form>
    </body>
</html>
