<%-- 
    Document   : ingresarMilitar
    Created on : 02/11/2019, 03:52:51
    Author     : Leandro
--%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Ingresar o modificar militar</title>
    </head>
    <body>

        <h1>
        Seleccione el tipo de militar a ingresar o modificar:
        </h1>
        <form method="post" action="ControladorIngresarMilitar">
        <p>
            <select name="tipo" size="1">
                <option value="Oficial"> Oficial </option>
                <option value="Suboficial"> Suboficial </option>
                <option value="Soldado"> Soldado </option>

            </select><br><br>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">

            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
        </p>
        </form> 
    </body>
</html>