<%-- 
    Document   : ingresarMilitar
    Created on : 02/11/2019, 03:52:51
    Author     : Leandro
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Ingresar o modificar soldado</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del soldado:
        </h1>
        <h2>
            Si existe un soldado con el código ingresado, se modificará.
            <br>
            Si no existe un soldado con el código ingresado, se agregará.
            
        </h2>
        <form method="post" action="ControladorIngresarSoldado">
            <table style="margin: 0 auto;">
                <tr>
                    <th>Codigo:</th>
                    <th><input type="number" name="codigo"> </th>
            </tr>
            <tr>
                <th>Contraseña:</th>
                <th> <input type="password" name="password"></th>
            </tr>
            <tr>
                <th>Nombre:</th>
                <th><input type="text" name="nombre"></th>
            </tr>
            <tr>
                <th>Apellido:</th>
                <th><input type="text" name="apellido"></th>
            </tr>
            <tr>
                <th>Graduación:</th>
                <th>
            <select name="graduacion" size="1">
                <option value="Voluntario de primera"> Voluntario de primera</option>
                <option value="Voluntario de segunda"> Voluntario de segunda </option>
                <option value="Voluntario de segunda en comision"> Voluntario de segunda en comision</option>
            </select>
                </th>
            </tr>
            <tr>
                <th>Compania: </th>
                <th>
            <select name="codCompania" size="1" >
                <c:forEach items="${companias}" var="compania" >
                    <option value="${compania.codigo}" align="center"> 
                        Codigo: ${compania.codigo} &nbsp; , &nbsp; 
                        Nombre: ${compania.actividad}
                    </option>
                </c:forEach>
            </select>
                </th>
            </tr>
            <tr>
               
                <th>Cuerpo: </th>
                <th>
            <select name="codCuerpo" size="1"  >
                <c:forEach items="${cuerpos}" var="cuerpo" >
                    <option value="${cuerpo.codigo}" align="center"> 
                        Codigo: ${cuerpo.codigo} &nbsp; , &nbsp; 
                        Nombre: ${cuerpo.denominacion}
                    </option>
                </c:forEach>       
            </select>
                </th>
            </tr>
            <tr>
                <th> Cuartel: </th>
                <th>
            <select name="codCuartel" size="1"  >
                <c:forEach items="${cuarteles}" var="cuartel" >
                    <option value="${cuartel.codigo}" align="center"> 
                        Codigo: ${cuartel.codigo} &nbsp; , &nbsp; 
                        Nombre: ${cuartel.nombre}
                    </option>
                </c:forEach>        
            
            </select>
                </th>
            </tr>
            </table>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
<br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form> 
    
    </body>
</html>