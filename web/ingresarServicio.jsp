<%-- 
    Document   : ingresarServicio
    Created on : 02/11/2019, 03:54:45
    Author     : Leandro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Ingresar o modificar servicio</title>
    </head>
    <body>
        <h1>
            Ingrese los datos del servicio:
        </h1>
        <h2>
            Si existe un servicio con el código ingresado, se modificará.
            <br>
            Si no existe un servicio con el código ingresado, se agregará.

        </h2>

        <form method="post" action="ControladorIngresarServicio">
            <table style="margin: 0 auto;">
                <tr>
                    <th>Codigo:</th>
                    <th><input type="number" name="codigo"></th>
                </tr>
                <tr>
                    <th>Descripcion:</th>
                    <th><input type="text" name="descripcion"></th>
                </tr>
            </table>
            <input type="hidden" name="dirIP" value="${dirIP}">
            <input type="hidden" name="nomBD" value="${nomBD}">
            <br>
            <input type="submit" style="width:110px; height:40px" value ="Enviar">  
            </p>
        </form> 
    </body>
</html>
