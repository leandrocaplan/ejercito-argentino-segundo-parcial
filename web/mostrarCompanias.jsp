<%-- 
    Document   : mostrarMilitares
    Created on : 03/11/2019, 01:26:58
    Author     : Leandro
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">

        <title>Muestro companias</title>
    </head>
        <body>


        <c:choose>
            <c:when test="${empty companias}">
                <h1>ERROR</h1>
                <p>
                    No existen compañías cargadas.
                </p>
            </c:when>
            <c:otherwise>
                <h1>
                    Lista de compañías:
                </h1>
                <table style="margin: 0 auto;">
                    
                <c:forEach var="compania" items="${companias}" >
                    <tr>
                    <th>
                        Codigo: ${compania.codigo} 
                    </th>
                    <th>
                        Actividad: ${compania.actividad} &nbsp;&nbsp;
                    </th>
                </tr>    
                </c:forEach>
                
                </table>
            </c:otherwise>
        </c:choose>
                    
    </body>
</html>
