-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 08-11-2019 a las 18:44:24
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `parcial`
--
CREATE DATABASE IF NOT EXISTS `parcial` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `parcial`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companias`
--

CREATE TABLE `companias` (
  `Codigo` int(11) NOT NULL,
  `Actividad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `companias`
--

INSERT INTO `companias` (`Codigo`, `Actividad`) VALUES
(44, 'Debugear'),
(55, 'Saraza'),
(311, 'Alguna'),
(444, 'Saltar'),
(888, 'Codificar'),
(1234, 'Estudiar'),
(5331, 'Inspeccion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuarteles`
--

CREATE TABLE `cuarteles` (
  `Codigo` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Ubicacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuarteles`
--

INSERT INTO `cuarteles` (`Codigo`, `Nombre`, `Ubicacion`) VALUES
(88, 'Patricios', 'Palermo'),
(98, 'Puerta 8', 'Podesta'),
(666, 'Campo de Mayo', 'San Miguel'),
(777, 'Jorge', 'Jimenez'),
(5538, 'Otro', 'Nuñez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuerpos`
--

CREATE TABLE `cuerpos` (
  `Codigo` int(11) NOT NULL,
  `Denominacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuerpos`
--

INSERT INTO `cuerpos` (`Codigo`, `Denominacion`) VALUES
(343, 'Saraza'),
(422, 'Hola'),
(555, 'Beta'),
(999, 'Caballeria'),
(3433, 'Artilleria'),
(9911, 'Infanteria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `militares`
--

CREATE TABLE `militares` (
  `Codigo` int(11) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Tipo` varchar(10) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Apellido` varchar(50) NOT NULL,
  `Graduacion` varchar(50) NOT NULL,
  `Compania` int(11) DEFAULT NULL,
  `Cuerpo` int(11) DEFAULT NULL,
  `Cuartel` int(11) DEFAULT NULL,
  `ServiciosRealizados` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `militares`
--

INSERT INTO `militares` (`Codigo`, `Password`, `Tipo`, `Nombre`, `Apellido`, `Graduacion`, `Compania`, `Cuerpo`, `Cuartel`, `ServiciosRealizados`) VALUES
(45, 'eee', 'Oficial', 'Matias', 'Pereyra', 'General de brigada', NULL, NULL, NULL, NULL),
(111, 'eee', 'Oficial', 'Palito', 'Ortega', 'Coronel', NULL, NULL, NULL, NULL),
(446, 'papa', 'Oficial', 'Pepe', 'Biondi', 'Coronel mayor', NULL, NULL, NULL, NULL),
(553, 'aaa', 'Soldado', 'Lisandro', 'De la Torre', 'Voluntario de primera', 44, 343, 88, NULL),
(555, 'qwerty', 'Soldado', 'Guillermo', 'Vega', 'Voluntario de segunda', 311, 3433, 98, NULL),
(665, 'qqq', 'Soldado', 'Matias', 'Avalos', 'Voluntario de segunda', 888, 555, 666, NULL),
(666, 'aaa', 'Soldado', 'Prueba', 'Bean', 'Voluntario de primera', 44, 343, 88, NULL),
(668, 'aaa', 'Soldado', 'Martin', 'Di Lella', 'Voluntario de segunda en comision', 311, 422, 5538, NULL),
(669, 'yyy', 'Soldado', 'Norberto', 'Napolitano', 'Voluntario de segunda', 311, 3433, 5538, NULL),
(676, 'rrr', 'Soldado', 'Alfredo', 'Casero', 'Voluntario de segunda', 311, 3433, 5538, NULL),
(777, 'aaa', 'Oficial', 'Jorge', 'Lopez', 'Teniente general', NULL, NULL, NULL, NULL),
(999, 'aaa', 'Oficial', 'Julio', 'Martinez', 'Coronel', NULL, NULL, NULL, NULL),
(1234, 'qwerty', 'Oficial', 'Lorenzo', 'Miguel', 'Mayor', NULL, NULL, NULL, NULL),
(3121, '222', 'Suboficial', 'Maria', 'Gomez', 'Suboficial Mayor', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `Codigo` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`Codigo`, `Descripcion`) VALUES
(232, 'Correr'),
(455, 'Lagartijas'),
(996, 'Barrer');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serviciosrealizados`
--

CREATE TABLE `serviciosrealizados` (
  `CodigoServicioRealizado` int(11) NOT NULL,
  `CodigoSoldado` int(11) NOT NULL,
  `CodigoServicio` int(11) NOT NULL,
  `Fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `serviciosrealizados`
--

INSERT INTO `serviciosrealizados` (`CodigoServicioRealizado`, `CodigoSoldado`, `CodigoServicio`, `Fecha`) VALUES
(11, 666, 996, '1985-04-21'),
(12, 555, 996, '2015-07-30'),
(15, 668, 996, '1998-08-30'),
(16, 668, 996, '1992-12-04'),
(17, 553, 232, '2019-11-12'),
(18, 553, 232, '2017-05-12'),
(19, 553, 232, '2017-10-30'),
(20, 553, 232, '2012-11-06'),
(21, 665, 455, '2019-11-04');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `companias`
--
ALTER TABLE `companias`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `cuarteles`
--
ALTER TABLE `cuarteles`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `cuerpos`
--
ALTER TABLE `cuerpos`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `militares`
--
ALTER TABLE `militares`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `serviciosrealizados`
--
ALTER TABLE `serviciosrealizados`
  ADD PRIMARY KEY (`CodigoServicioRealizado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `serviciosrealizados`
--
ALTER TABLE `serviciosrealizados`
  MODIFY `CodigoServicioRealizado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
