package DAO;


import Beans.SoldadoBean;
import Beans.CompaniaBean;
import Beans.ServicioRealizadoBean;
import Beans.CuerpoBean;
import Beans.SuboficialBean;
import Beans.OficialBean;
import Beans.ServicioBean;
import Beans.MilitarBean;
import Beans.CuartelBean;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;

public class ModeloDAO {

    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private MilitarBean militarLogin;
    private String tipoLogin;
    private ArrayList<MilitarBean> militares;
    private ArrayList<CompaniaBean> companias;
    private ArrayList<CuerpoBean> cuerpos;
    private ArrayList<CuartelBean> cuarteles;
    private ArrayList<ServicioBean> servicios;
    private ArrayList<ServicioRealizadoBean> serviciosRealizados;
    private ActionListener listener;

    public ModeloDAO(String url, String dbName) {
        ////JOptionhowMessageDialog(null, "Cargo el modelo\n" + url + "\n" + dbName);
        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        listener = null;
        //resultado = new ArrayList<BAEcoParqueBean>();
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }

    public boolean login(String cod, String pass) {
        boolean valido = false;
        try {
            //   ////JOptionhowMessageDialog(null, "Entro a login\n");
            //  ////JOptionhowMessageDialog(null, urlRoot + dbName);
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            //   ////JOptionhowMessageDialog(null, con);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM militares WHERE Codigo='" + cod + "' AND Password='" + pass + "';");
            //ResultSet rs = stmt.getResultSet();
            //Se genera un Objeto del tipo ResultSet que representará los resultados de nuestra básqueda;
            //el inicio de la búsqueda es iniciado por medio del método execute perteneciente a la Clase Statement.
            while (rs.next()) {
                //     ////JOptionhowMessageDialog(null, "Codigo militar encontrado: " + rs.getString("Codigo"));
                //    ////JOptionhowMessageDialog(null, "Password militar encontrada: " + rs.getString("Password"));
                if (cod.equals(rs.getString("Codigo")) && pass.equals(rs.getString("Password"))) {

                    valido = true;
                    this.tipoLogin = rs.getString("Tipo");
                    switch (tipoLogin) {
                        case ("Oficial"):
                            militarLogin = new OficialBean(
                                    rs.getInt("Codigo"),
                                    rs.getString("Tipo"),
                                    rs.getString("Password"),
                                    rs.getString("Nombre"),
                                    rs.getString("Apellido"),
                                    rs.getString("Graduacion"));
                            break;
                        case ("Suboficial"):
                            militarLogin = new SuboficialBean(
                                    rs.getInt("Codigo"),
                                    rs.getString("Tipo"),
                                    rs.getString("Password"),
                                    rs.getString("Nombre"),
                                    rs.getString("Apellido"),
                                    rs.getString("Graduacion"));
                            break;
                        case ("Soldado"):
                            militarLogin = new SoldadoBean(
                                    rs.getInt("Codigo"),
                                    rs.getString("Tipo"),
                                    rs.getString("Password"),
                                    rs.getString("Nombre"),
                                    rs.getString("Apellido"),
                                    rs.getString("Graduacion"),
                                    rs.getInt("Compania"),
                                    rs.getInt("Cuerpo"),
                                    rs.getInt("Cuartel"));
                            break;
                    }
                }
            }
            con.close();
        } catch (SQLException e) {
            //JOptionhowMessageDialog(null, "Entro al catch de login " + e.getMessage());

            reportException(e.getMessage());
        }
        return valido;
    }

    public MilitarBean getMilitarLogin() {
        return this.militarLogin;
    }

    public String getTipo() {
        return this.tipoLogin;
    }

    public void bajaMilitar(String codigo) throws SQLException {
        try {
            String query = "DELETE FROM militares WHERE codigo=" + codigo;
            ////JOptionhowMessageDialog(null, query);
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de bajaMilitar " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void bajaCompania(String codigo) throws SQLException {
        try {
            String query = "DELETE FROM companias WHERE codigo=" + codigo;
            ////JOptionhowMessageDialog(null, query);
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de bajaCompania " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void bajaCuerpo(String codigo) throws SQLException {
        try {
            String query = "DELETE FROM cuerpos WHERE codigo=" + codigo;
            ////JOptionhowMessageDialog(null, query);
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de bajaCuerpo " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void bajaCuartel(String codigo) throws SQLException {
        try {
            String query = "DELETE FROM cuarteles WHERE codigo=" + codigo;
            ////JOptionhowMessageDialog(null, query);
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de bajaCuartel " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void bajaServicio(String codigo) throws SQLException {
        try {
            String query = "DELETE FROM servicios WHERE codigo=" + codigo;
            ////JOptionhowMessageDialog(null, query);
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de bajaServicio " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void desasignarServicio(String codigo) throws SQLException {
        try {
            String query = "DELETE FROM serviciosrealizados WHERE CodigoServicioRealizado=" + codigo;
            ////JOptionhowMessageDialog(null, query);
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de desasigar servicio" + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void cargarMilitares() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.militares = new ArrayList<MilitarBean>();
            String query = "SELECT * FROM militares";
            // ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            while (resultSet.next()) {
                //          ////JOptionhowMessageDialog(null, "Entro al while");
                String tipo = resultSet.getString("Tipo");
                //              ////JOptionhowMessageDialog(null, tipo);
                MilitarBean militar = null;
                if (tipo.equals("Oficial")) {
                    // ////JOptionhowMessageDialog(null, "Entro al if");
                    militar = new OficialBean(
                            resultSet.getInt("Codigo"),
                            resultSet.getString("Tipo"),
                            resultSet.getString("Password"),
                            resultSet.getString("Nombre"),
                            resultSet.getString("Apellido"),
                            resultSet.getString("Graduacion"));
                    //                ////JOptionhowMessageDialog(null, "Salgo del if");
                }
                //          ////JOptionhowMessageDialog(null, militar.toString());
                if (tipo.equals("Suboficial")) {
                    militar = new SuboficialBean(
                            resultSet.getInt("Codigo"),
                            resultSet.getString("Tipo"),
                            resultSet.getString("Password"),
                            resultSet.getString("Nombre"),
                            resultSet.getString("Apellido"),
                            resultSet.getString("Graduacion"));
                }
                if (tipo.equals("Soldado")) {
                    militar = new SoldadoBean(
                            resultSet.getInt("Codigo"),
                            resultSet.getString("Tipo"),
                            resultSet.getString("Password"),
                            resultSet.getString("Nombre"),
                            resultSet.getString("Apellido"),
                            resultSet.getString("Graduacion"),
                            resultSet.getInt("Compania"),
                            resultSet.getInt("Cuerpo"),
                            resultSet.getInt("Cuartel"));
                }
                //           ////JOptionhowMessageDialog(null, "Entro al add");
                this.militares.add(militar);
                //       ////JOptionhowMessageDialog(null, "Ejecute el add");
            }
            //       ////JOptionhowMessageDialog(null, "Salgo del while");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de cargarMilitares " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void cargarCompanias() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.companias = new ArrayList<CompaniaBean>();
            String query = "SELECT * FROM companias";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            while (resultSet.next()) {
                //          ////JOptionhowMessageDialog(null, "Entro al while");

                //              ////JOptionhowMessageDialog(null, tipo);
                CompaniaBean compania = new CompaniaBean(resultSet.getInt("Codigo"), resultSet.getString("Actividad"));
                this.companias.add(compania);
                //       ////JOptionhowMessageDialog(null, "Ejecute el add");
            }
            //       ////JOptionhowMessageDialog(null, "Salgo del while");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de cargarCompanias " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void cargarCuerpos() {
        try {
            ////JOptionhowMessageDialog(null, "Entro a cargarCuerpos");
            this.cuerpos = new ArrayList<CuerpoBean>();
            String query = "SELECT * FROM cuerpos";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            while (resultSet.next()) {
                //    ////JOptionhowMessageDialog(null, "Entro al while");

                //      ////JOptionhowMessageDialog(null, tipoLogin);
                CuerpoBean cuerpo = new CuerpoBean(resultSet.getInt("Codigo"), resultSet.getString("Denominacion"));
                this.cuerpos.add(cuerpo);
                //     ////JOptionhowMessageDialog(null, "Ejecute el add");
            }
            ////JOptionhowMessageDialog(null, "Salgo del while");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de cargarCuerpos " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void cargarServicios() {
        try {
            ////JOptionhowMessageDialog(null, "Entro a cargarServicios");
            this.servicios = new ArrayList<ServicioBean>();
            String query = "SELECT * FROM servicios";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            while (resultSet.next()) {
                // ////JOptionhowMessageDialog(null, "Entro al while");
                ServicioBean servicio = new ServicioBean(resultSet.getInt("Codigo"), resultSet.getString("Descripcion"));
                this.servicios.add(servicio);
                // ////JOptionhowMessageDialog(null, "Ejecute el add");
            }
            ////JOptionhowMessageDialog(null, "Salgo del while");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de cargarServicios " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void cargarServiciosRealizados() {
        try {
            ////JOptionhowMessageDialog(null, "Entro a cargarServiciosRealizados");
            this.serviciosRealizados = new ArrayList<ServicioRealizadoBean>();
            String query = "SELECT * FROM serviciosRealizados";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            while (resultSet.next()) {
                ////JOptionhowMessageDialog(null, "Entro al while");
                ServicioRealizadoBean servicioRealizado
                        = new ServicioRealizadoBean(resultSet.getInt("CodigoServicioRealizado"), resultSet.getInt("CodigoSoldado"),
                                resultSet.getInt("CodigoServicio"), resultSet.getDate("Fecha").toString());
                //     ////JOptionhowMessageDialog(null, servicioRealizado);
                this.serviciosRealizados.add(servicioRealizado);
                //   ////JOptionhowMessageDialog(null, "Ejecute el add");
            }
            //////JOptionhowMessageDialog(null, this.serviciosRealizados);
            ////JOptionhowMessageDialog(null, "Salgo del while");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de cargarServiciosRealizados " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public ArrayList<ServicioRealizadoBean> getServiciosRealizados() {
        return serviciosRealizados;
    }

    public ArrayList<CompaniaBean> getCompanias() {
        return this.companias;
    }

    public ArrayList<CuerpoBean> getCuerpos() {
        return this.cuerpos;
    }

    public ArrayList<CuartelBean> getCuarteles() {
        return this.cuarteles;
    }

    public ArrayList<ServicioBean> getServicios() {
        return this.servicios;
    }

    public void cargarCuarteles() {
        try {
            ////JOptionhowMessageDialog(null, "Entro a cargarCuarteles");
            this.cuarteles = new ArrayList<CuartelBean>();
            String query = "SELECT * FROM cuarteles";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            while (resultSet.next()) {
                //    ////JOptionhowMessageDialog(null, "Entro al while");

                //    ////JOptionhowMessageDialog(null, resultSet.getInt("Codigo") + resultSet.getString("Nombre")
                //           + resultSet.getString("Ubicacion"));
                CuartelBean cuartel = new CuartelBean(resultSet.getInt("Codigo"), resultSet.getString("Nombre"),
                        resultSet.getString("Ubicacion"));
                this.cuarteles.add(cuartel);
                //  ////JOptionhowMessageDialog(null, "Ejecute el add");
            }
            ////JOptionhowMessageDialog(null, "Salgo del while");
            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de cargarCuarteles " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public ArrayList<MilitarBean> getMilitares() {
        return this.militares;
    }

    public void ingresarOficial(String codigo, String password, String nombre, String apellido, String graduacion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarOficial");
            String tipo = "Oficial";
            String query = "INSERT INTO militares(Codigo,Password,Tipo,Nombre,Apellido,Graduacion)"
                    + " VALUES (" + codigo + ",'" + password + "','" + tipo + "','" + nombre + "','" + apellido
                    + "','" + graduacion + "')";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarOficial " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void ingresarSuboficial(String codigo, String password, String nombre, String apellido, String graduacion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarSuboficial");
            String tipo = "Suboficial";
            String query = "INSERT INTO militares(Codigo,Password,Tipo,Nombre,Apellido,Graduacion)"
                    + " VALUES (" + codigo + ",'" + password + "','" + tipo + "','" + nombre + "','" + apellido
                    + "','" + graduacion + "')";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarSuboficial  " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void ingresarSoldado(String codigo, String password, String nombre,
            String apellido, String graduacion, String codCompania, String codCuerpo, String codCuartel) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarSoldado");
            String tipo = "Soldado";
            String query = "INSERT INTO militares(Codigo,Password,Tipo,Nombre,Apellido,Graduacion,Compania,Cuerpo,Cuartel)"
                    + " VALUES (" + codigo + ",'" + password + "','" + tipo + "','" + nombre + "','" + apellido
                    + "','" + graduacion + "'," + codCompania + "," + codCuerpo + "," + codCuartel + ")";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarSoldado " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void ingresarCompania(String codigo, String actividad) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarCompania");
            String query = "INSERT INTO companias(Codigo,Actividad)"
                    + " VALUES (" + codigo + ",'" + actividad + "')";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarCompania " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void ingresarCuerpo(String codigo, String denominacion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarCuerpo");
            String query = "INSERT INTO cuerpos(Codigo,Denominacion)"
                    + " VALUES (" + codigo + ",'" + denominacion + "')";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarCuerpo " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void ingresarCuartel(String codigo, String nombre, String ubicacion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarCuartel");
            String query = "INSERT INTO cuarteles(Codigo,Nombre,Ubicacion)"
                    + " VALUES (" + codigo + ",'" + nombre + "','" + ubicacion + "')";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            ////JOptionhowMessageDialog(null, "Obtuve la conexion");

            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarCuartel " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void ingresarServicio(String codigo, String descripcion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarServicio");

            String query = "INSERT INTO servicios(Codigo,Descripcion)"
                    + " VALUES (" + codigo + ",'" + descripcion + "')";
            ////JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarServicio " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    public void modificarOficial(String codigo, String password, String nombre, String apellido, String graduacion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarOficial");
            String tipo = "Oficial";
            String query = "UPDATE militares SET "
                    + "Password = '" + password + "'"
                    + ",Tipo ='" + tipo + "'"
                    + ",Nombre='" + nombre + "'"
                    + ",Apellido='" + apellido + "'"
                    + ",Graduacion='" + graduacion + "'"
                    + "WHERE Codigo=" + codigo;
            ;
            //JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarOficial " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }
    public void modificarSuboficial(String codigo, String password, String nombre, String apellido, String graduacion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarOficial");
            String tipo = "Suboficial";
            String query = "UPDATE militares SET "
                    + "Password = '" + password + "'"
                    + ",Tipo ='" + tipo + "'"
                    + ",Nombre='" + nombre + "'"
                    + ",Apellido='" + apellido + "'"
                    + ",Graduacion='" + graduacion + "'"
                    + "WHERE Codigo=" + codigo;
            ;
            //JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarOficial " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }
    
    public void modificarSoldado(String codigo, String password, String nombre,
            String apellido, String graduacion, String codCompania,
            String codCuerpo, String codCuartel) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarOficial");
           //Codigo,Password,Tipo,Nombre,Apellido,Graduacion,Compania,Cuerpo,Cuartel
            String tipo = "Soldado";
            String query = "UPDATE militares SET "
                    + "Password = '" + password + "'"
                    + ",Tipo ='" + tipo + "'"
                    + ",Nombre='" + nombre + "'"
                    + ",Apellido='" + apellido + "'"
                    + ",Graduacion='" + graduacion + "'"
                    + ",Compania=" + codCompania  
                    + ",Cuerpo=" + codCuerpo
                    + ",Cuartel=" + codCuartel
                    + " WHERE Codigo=" + codigo;
            ;
            //JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarOficial " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }
 public void modificarCompania(String codigo, String actividad) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarOficial");
           //Codigo,Password,Tipo,Nombre,Apellido,Graduacion,Compania,Cuerpo,Cuartel
            String query = "UPDATE companias SET "
                    + "Actividad = '" + actividad + "'"
                    + " WHERE Codigo=" + codigo;
            ;
            //JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarOficial " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }
  public void modificarCuerpo(String codigo, String denominacion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarOficial");
           //Codigo,Password,Tipo,Nombre,Apellido,Graduacion,Compania,Cuerpo,Cuartel
            String query = "UPDATE cuerpos SET "
                    + "Denominacion = '" + denominacion + "'"
                    + " WHERE Codigo=" + codigo;
            ;
            //JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarOficial " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }
   public void modificarCuartel(String codigo, String nombre, String ubicacion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarOficial");
           //Codigo,Password,Tipo,Nombre,Apellido,Graduacion,Compania,Cuerpo,Cuartel
            String query = "UPDATE cuarteles SET "
                    + "Nombre = '" + nombre + "',"
                    + "Ubicacion = '" + ubicacion + "'"
                    + " WHERE Codigo=" + codigo;
            ;
            //JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarOficial " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }
   public void modificarServicio(String codigo, String descripcion) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarOficial");
           //Codigo,Password,Tipo,Nombre,Apellido,Graduacion,Compania,Cuerpo,Cuartel
            String query = "UPDATE servicios SET "
                    + "Descripcion = '" + descripcion + "'"
                    + " WHERE Codigo=" + codigo;
            ;
            //JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de ingresarOficial " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }
    public void asignarServicio(String codSoldado, String codServicio, String fecha) throws SQLException {
        try {
            ////JOptionhowMessageDialog(null, "Entro a ingresarServicio");

            String query = "INSERT INTO serviciosrealizados(CodigoSoldado,CodigoServicio,Fecha)"
                    + " VALUES (" + codSoldado + "," + codServicio + ","
                    + "'" + fecha + "')";
            //JOptionhowMessageDialog(null, query);

            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            ////JOptionhowMessageDialog(null, "Ejecute la query");

            con.close();
        } catch (SQLException ex) {
            //JOptionhowMessageDialog(null, "Entro al catch de asignarServicio " + ex.getMessage());
            reportException(ex.getMessage());
        }
    }

    private void reportException(String exception) {
        if (listener != null) {
            //JOptionhowMessageDialog(null, exception);
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

}
