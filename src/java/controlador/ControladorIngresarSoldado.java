package controlador;


import Beans.MilitarBean;
import DAO.ModeloDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Leandro
 */
public class ControladorIngresarSoldado extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        boolean valido = false;
        //JOptionhowMessageDialog(null, "Entro al doPost");
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");

        String codigo = request.getParameter("codigo");
        String password = request.getParameter("password");
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String graduacion = request.getParameter("graduacion");
        String codCompania = request.getParameter("codCompania");
        String codCuerpo = request.getParameter("codCuerpo");
        String codCuartel = request.getParameter("codCuartel");
        //JOptionhowMessageDialog(null, codigo + " " + password + " "
  //              + nombre + " " + apellido + " " + graduacion + " " + codCompania + " " + codCuerpo
    //            + " " + codCuartel);

        ModeloDAO m = new ModeloDAO(ip, bd);
        m.addExceptionListener(new ControladorIngresarSoldado.ExceptionListener());

        RequestDispatcher vista = null;

        try {

            boolean existe = false;
            boolean esSoldado = false;
            m.cargarMilitares();
            for (MilitarBean militar : m.getMilitares()) {
                if (militar.getCodigo().toString().equals(codigo)) {
                    existe = true;
                    if (militar.getTipo().equals("Soldado")) {
                        //JOptionhowMessageDialog(null, "Es soldado");
                        esSoldado = true;
                    }
                }
            }
            if (!existe) {
                m.ingresarSoldado(codigo, password, nombre, apellido, graduacion, codCompania, codCuerpo, codCuartel);
                vista = request.getRequestDispatcher("altaExitosa.jsp");
            } else if (esSoldado) {
                m.modificarSoldado(codigo, password, nombre, apellido, graduacion, codCompania, codCuerpo, codCuartel);
                vista = request.getRequestDispatcher("altaExitosa.jsp");
                // request.setAttribute("mensajeError", "El codigo de militar ingresado ya existe");
            } else {
                request.setAttribute("mensajeError", "El codigo de militar ingresado existe y no corresponde a un soldado");
                vista = request.getRequestDispatcher("vistaError.jsp");
            }

            vista.forward(request, response);

        } catch (Exception e) {
            //JOptionhowMessageDialog(null, e);
            request.setAttribute("mensajeError", e.getMessage());
            vista = request.getRequestDispatcher("vistaError.jsp");
            vista.forward(request, response);
        }
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();

            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
