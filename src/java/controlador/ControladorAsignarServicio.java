package controlador;

import DAO.ModeloDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Leandro
 */
public class ControladorAsignarServicio extends HttpServlet {
    HttpServletRequest request;
    HttpServletResponse response;
    
    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
         throws IOException, ServletException {
        
        boolean valido = false;
        //JOptionPane.showMessageDialog(null, "Entro al doPost");
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");
        
        String codSoldado = request.getParameter("codSoldado");
        String codServicio = request.getParameter("codServicio");
        String fecha = request.getParameter("fecha");
        
        //JOptionPane.showMessageDialog(null, codSoldado+ " " +codServicio + " " + fecha);

        ModeloDAO m = new ModeloDAO(ip, bd);
        m.addExceptionListener(new ControladorAsignarServicio.ExceptionListener());

        RequestDispatcher vista = null;

        try {
            //JOptionPane.showMessageDialog(null,fecha);
           // m.ingresarSoldado(codigo, password, nombre, apellido, graduacion,codCompania,codCuerpo,codCuartel);
            m.asignarServicio(codSoldado, codServicio, fecha);
            vista = request.getRequestDispatcher("altaExitosa.jsp");
            vista.forward(request, response);

        }
         catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e);
            request.setAttribute("mensajeError", e.getMessage());
            vista = request.getRequestDispatcher("vistaError.jsp");
            vista.forward(request, response);
        }
    }
    
    
private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }    
}

