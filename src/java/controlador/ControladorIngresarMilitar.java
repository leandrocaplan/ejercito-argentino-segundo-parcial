package controlador;


import DAO.ModeloDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Leandro
 */
public class ControladorIngresarMilitar extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        boolean valido = false;
        //JOptionowMessageDialog(null, "Entro al doPost");
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");

        String tipo = request.getParameter("tipo");

        //JOptionowMessageDialog(null, tipo);

        ModeloDAO m = new ModeloDAO(ip, bd);
        m.addExceptionListener(new ControladorIngresarMilitar.ExceptionListener());

        RequestDispatcher vista = null;

        try {

            request.setAttribute("dirIP", ip);
            request.setAttribute("nomBD", bd);
            switch (tipo) {
                case "Oficial":
                    vista = request.getRequestDispatcher("ingresarOficial.jsp");
                    break;
                case "Suboficial":
                    vista = request.getRequestDispatcher("ingresarSuboficial.jsp");
                    break;
                case "Soldado":
                    String faltante = "";
                    boolean faltaElemento = false;

                    m.cargarCompanias();
                    m.cargarCuerpos();
                    m.cargarCuarteles();

                    if (m.getCompanias().isEmpty()) {
                        faltaElemento = true;
                        faltante = "companias";
                    }
                    else if (m.getCuerpos().isEmpty()) {
                        faltaElemento = true;
                        faltante = "cuerpos";
                    }
                    else if (m.getCuarteles().isEmpty()) {
                        faltaElemento = true;
                        faltante = "cuarteles";
                    }
                    if (!faltaElemento) {
                        request.setAttribute("companias", m.getCompanias());
                        request.setAttribute("cuerpos", m.getCuerpos());
                        request.setAttribute("cuarteles", m.getCuarteles());
                        vista = request.getRequestDispatcher("ingresarSoldado.jsp");
                    }
                    else{
                   request.setAttribute("mensajeError","No se puede dar de alta un soldado, ya que no hay " + faltante +" disponibles");
                        vista = request.getRequestDispatcher("vistaError.jsp");
                    }
                    
                    break;
            }

            vista.forward(request, response);

        } catch (Exception e) {
            //JOptionowMessageDialog(null, e);
            request.setAttribute("mensajeError", e.getMessage());
            vista = request.getRequestDispatcher("vistaError.jsp");
            vista.forward(request, response);
        }
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();

            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
