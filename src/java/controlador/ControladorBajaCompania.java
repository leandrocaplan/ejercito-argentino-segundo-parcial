package controlador;


import Beans.MilitarBean;
import Beans.SoldadoBean;
import DAO.ModeloDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Leandro
 */
public class ControladorBajaCompania extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        //JOptionPane.showMessageDialog(null, "Entro al doPost");
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");

        String codigo = request.getParameter("companiaBaja");

        //JOptionPane.showMessageDialog(null, codigo);

        ModeloDAO m = new ModeloDAO(ip, bd);
        m.addExceptionListener(new ControladorBajaCompania.ExceptionListener());

        RequestDispatcher vista = null;

        try {
            boolean cargado = false;
            m.cargarMilitares();

            for (MilitarBean militar : m.getMilitares()) {
                //JOptionPane.showMessageDialog(null, "Entro al for");
                if (militar instanceof SoldadoBean) {
                    //JOptionPane.showMessageDialog(null, militar);
                    SoldadoBean soldado = (SoldadoBean) militar;
                    if (soldado.getCodCompania().toString().equals(codigo)) {
                        cargado = true;
                    }
                }
            }

            if (!cargado) {
                m.bajaCompania(codigo);
                vista = request.getRequestDispatcher("bajaExitosa.jsp");
            } else {
                request.setAttribute("mensajeError", "No se puede dar de baja la compania, ya que está asignado a un soldado");
                vista = request.getRequestDispatcher("vistaError.jsp");
            }
            vista.forward(request, response);

        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e);
            request.setAttribute("mensajeError", e.getMessage());
            vista = request.getRequestDispatcher("vistaError.jsp");
            vista.forward(request, response);
        }
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();

            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
