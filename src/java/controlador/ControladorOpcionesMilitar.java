package controlador;


import DAO.ModeloDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ControladorOpcionesMilitar extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        boolean valido = false;
        //JOptionhowMessageDialog(null, "Entro al doPost");
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");
        String codigo = request.getParameter("codigo");
        String password = request.getParameter("password");
        String opcion = request.getParameter("opcion");
        ModeloDAO m = new ModeloDAO(ip, bd);
        m.addExceptionListener(new ExceptionListener());

        RequestDispatcher vista = null;

        try {
            request.setAttribute("dirIP", ip);
            request.setAttribute("nomBD", bd);
            switch (opcion) {

                case "mostrar_militares":
                    /*m.cargarServiciosRealizados();
                    //JOptionhowMessageDialog(null, "Cargue servicios realizados");
                    */
                    m.cargarMilitares();
                    m.cargarCompanias();
                    m.cargarCuerpos();
                    m.cargarCuarteles();
                    m.cargarServicios();
                    m.cargarServiciosRealizados();
                    
                   // m.cargarServicios();
                    //JOptionhowMessageDialog(null, "Cargue hashmaps");
                    //Debug
                    //JOptionhowMessageDialog(null, m.getMilitares());
                   //JOptionhowMessageDialog(null, m.getCompanias());
                   //JOptionhowMessageDialog(null, m.getCuerpos()); 
                   //JOptionhowMessageDialog(null, m.getCuarteles());
                   //JOptionhowMessageDialog(null, m.getServicios());
                   //JOptionhowMessageDialog(null, m.getServiciosRealizados());
            
                   request.setAttribute("militares", m.getMilitares());
                    request.setAttribute("companias", m.getCompanias());
                    request.setAttribute("cuerpos", m.getCuerpos());
                    request.setAttribute("cuarteles", m.getCuarteles());
                    request.setAttribute("servicios", m.getServicios());
                    request.setAttribute("serviciosRealizados", m.getServiciosRealizados());
                    //JOptionhowMessageDialog(null, "Setee los atributos");
                    vista = request.getRequestDispatcher("mostrarMilitares.jsp");
                    //vista.forward(request, response);
                    break;
                    
                case "mostrar_companias":
                    m.cargarCompanias();
                    //JOptionhowMessageDialog(null, m.getCompanias());
                    request.setAttribute("companias", m.getCompanias());

                    vista = request.getRequestDispatcher("mostrarCompanias.jsp");
                    //vista.forward(request, response);
                    break;

                case "mostrar_cuerpos":
                    m.cargarCuerpos();
                    //JOptionhowMessageDialog(null, m.getCuerpos());
                    request.setAttribute("cuerpos", m.getCuerpos());

                    vista = request.getRequestDispatcher("mostrarCuerpos.jsp");

                    break;

                case "mostrar_cuarteles":
                    m.cargarCuarteles();
                    //JOptionhowMessageDialog(null, m.getCuarteles());
                    request.setAttribute("cuarteles", m.getCuarteles());

                    vista = request.getRequestDispatcher("mostrarCuarteles.jsp");
                    
                    break;

                case "mostrar_servicios":
                    m.cargarServicios();
                    //JOptionhowMessageDialog(null, m.getServicios());
                    request.setAttribute("servicios", m.getServicios());

                    vista = request.getRequestDispatcher("mostrarServicios.jsp");

                    break;

                case "alta_militar":

                    vista = request.getRequestDispatcher("ingresarMilitar.jsp");
                    break;

                case "alta_compania":

                    vista = request.getRequestDispatcher("ingresarCompania.jsp");
                    break;

                case "alta_cuerpo":
                    vista = request.getRequestDispatcher("ingresarCuerpo.jsp");
                    break;

                case "alta_cuartel":
                    vista = request.getRequestDispatcher("ingresarCuartel.jsp");
                    break;

                case "alta_servicio":
                    vista = request.getRequestDispatcher("ingresarServicio.jsp");
                    break;
                
                case "baja_militar":
                    m.cargarMilitares();
                    request.setAttribute("militares", m.getMilitares());

                    vista = request.getRequestDispatcher("bajaMilitar.jsp");
                    break;

                case "baja_compania":
                    m.cargarCompanias();
                    request.setAttribute("companias", m.getCompanias());
                    vista = request.getRequestDispatcher("bajaCompania.jsp");
                    break;
                case "baja_cuerpo":
                   m.cargarCuerpos();
                    request.setAttribute("cuerpos", m.getCuerpos());
                    vista = request.getRequestDispatcher("bajaCuerpo.jsp");
                    break;
                case "baja_cuartel":
                    m.cargarCuarteles();
                    request.setAttribute("cuarteles", m.getCuarteles());
                    vista = request.getRequestDispatcher("bajaCuartel.jsp");
                    break;
                case "baja_servicio":
                    m.cargarServicios();
                    request.setAttribute("servicios", m.getServicios());
                    vista = request.getRequestDispatcher("bajaServicio.jsp");
                    break;

                case "asignar_servicio":
                    m.cargarServicios();
                    m.cargarMilitares();
                    request.setAttribute("servicios", m.getServicios());
                    request.setAttribute("militares", m.getMilitares());
                    vista = request.getRequestDispatcher("asignarServicio.jsp");
                    break;

                case "desasignar_servicio":
                     m.cargarMilitares();
                    m.cargarServicios();
                    m.cargarServiciosRealizados();
                    request.setAttribute("militares", m.getMilitares());
                    request.setAttribute("servicios", m.getServicios());
                    request.setAttribute("serviciosRealizados", m.getServiciosRealizados());
                    vista = request.getRequestDispatcher("desasignarServicio.jsp");
                    break;
             
                case "logout":
                    vista = request.getRequestDispatcher("index.jsp");
                    break;
                default:
                    vista = request.getRequestDispatcher("faltaPagina.jsp");
                    break;

            }
            //JOptionhowMessageDialog(null, "Salgo del switch");
            vista.forward(request, response);
        } catch (Exception e) {
            //JOptionhowMessageDialog(null, e);
            request.setAttribute("mensajeError", e.getMessage());
            vista = request.getRequestDispatcher("vistaError.jsp");
            vista.forward(request, response);
        }
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
