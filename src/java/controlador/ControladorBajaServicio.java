package controlador;


import Beans.ServicioRealizadoBean;
import DAO.ModeloDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Leandro
 */
public class ControladorBajaServicio extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        //JOptionowMessageDialog(null, "Entro al doPost");
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");

        String codigo = request.getParameter("servicioBaja");

        //JOptionowMessageDialog(null, codigo);

        ModeloDAO m = new ModeloDAO(ip, bd);
        m.addExceptionListener(new ControladorBajaServicio.ExceptionListener());

        RequestDispatcher vista = null;

        try {
            boolean cargado = false;
            m.cargarServicios();
            m.cargarServiciosRealizados();

            for (ServicioRealizadoBean sr : m.getServiciosRealizados()) {
                //JOptionowMessageDialog(null, "Entro al for");
                //JOptionowMessageDialog(null, sr);
                if (sr.getCodServicio().toString().equals(codigo)) {
                    cargado = true;
                }
            }
        

        if (!cargado) {
            m.bajaServicio(codigo);
            vista = request.getRequestDispatcher("bajaExitosa.jsp");
        } else {
                request.setAttribute("mensajeError", "No se puede dar de baja el servicio, ya que está asignado a un soldado");
                vista = request.getRequestDispatcher("vistaError.jsp");
            }
        vista.forward(request, response);

    }
    catch (Exception e) {
            //JOptionowMessageDialog(null, e);
        request.setAttribute("mensajeError", e.getMessage());
        vista = request.getRequestDispatcher("vistaError.jsp");
        vista.forward(request, response);
    }
}

private class ExceptionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent event) {
        String exception = event.getActionCommand();

        request.setAttribute("mensajeError", exception);
        RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
        try {
            vista.forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
}
