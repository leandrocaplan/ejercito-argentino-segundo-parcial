package controlador;


import DAO.ModeloDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ControladorLogin extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        boolean valido = false;
        //////JOptionhowMessageDialog(null, "Entro al doPost");
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");
        String codigo = request.getParameter("codigo");
        String password = request.getParameter("password");
        ModeloDAO m = new ModeloDAO(ip, bd);
        m.addExceptionListener(new ExceptionListener());
        valido = m.login(codigo, password);

        RequestDispatcher vista = null;

        try {
          //  ////JOptionhowMessageDialog(null, "Entro al try");
            if (valido) {
                request.setAttribute("dirIP",ip);
                request.setAttribute("nomBD",bd);
                request.setAttribute("graduacion", m.getMilitarLogin().getGraduacion());
                
                request.setAttribute("graduacion", m.getMilitarLogin().getGraduacion());
                request.setAttribute("nombre", m.getMilitarLogin().getNombre());
                request.setAttribute("apellido", m.getMilitarLogin().getApellido());
                switch (m.getTipo()) {
                    case "Oficial":
                        vista = request.getRequestDispatcher("vistaOficial.jsp");
                        break;

                    case "Suboficial":
                        vista = request.getRequestDispatcher("vistaSuboficial.jsp");
                        break;

                    case "Soldado":
                        vista = request.getRequestDispatcher("vistaSoldado.jsp");
                        break;
                    default:
                        vista = request.getRequestDispatcher("vistaError.jsp");
                        break;
                }
                //vista = request.getRequestDispatcher("vistaOficial.jsp");
            } else {
               request.setAttribute("mensajeError", "Código o contraseña inválidos");  
                vista = request.getRequestDispatcher("vistaError.jsp");
            }

        //    ////JOptionhowMessageDialog(null, "Cargo el requestDispatcher");
        //    ////JOptionhowMessageDialog(null, vista);
          
            vista.forward(request, response);
        } catch (Exception e) {
            ////JOptionhowMessageDialog(null, e);
            request.setAttribute("mensajeError", e.getMessage());
            vista = request.getRequestDispatcher("vistaError.jsp");
            vista.forward(request, response);
        }
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("vistaError.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
