package Beans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Leandro
 */
public class ServicioBean  {
    private Integer codigo;
    private String descripcion;

    
    public ServicioBean(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;

    }

    public ServicioBean() {
     
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
    @Override
    public String toString() {
        return "Servicio{" + "codigo=" + codigo + ", descripcion=" + descripcion + '}';
    }
    
}