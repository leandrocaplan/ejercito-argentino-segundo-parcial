package Beans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


/**
 *
 * @author Leandro
 */
public class SoldadoBean extends MilitarBean{

      //Uso los códigos en lugar de la descripción
  
    private Integer codCuerpo;
    private Integer codCompania;
    private Integer codCuartel;
  
    //private ArrayList<ServicioRealizado> serviciosReailzados = new ArrayList<ServicioRealizado>();
    //private HashMap<Integer,Date> servicios = new ArrayList<Integer,Date>();
    
    
    @Deprecated
    public SoldadoBean(Integer codigo,String tipo,String password, String nombre, String apellido, String graduacion) {
        super(codigo,tipo,password,nombre,apellido,graduacion);
    }

    public SoldadoBean(Integer codigo,String tipo,String password, String nombre, String apellido, String graduacion, Integer codCompania,Integer codCuerpo, Integer codCuartel) {
        super(codigo,tipo,password, nombre, apellido, graduacion);

        this.codCuerpo = codCuerpo;
        this.codCompania = codCompania;
        this.codCuartel = codCuartel;
    }
    /*
    public Soldado(int codigo,String password, String nombre, String apellido, String graduacion,int codCuerpo, int codCompania, int codCuartel, ArrayList<ServicioRealizado> serviciosRealizados) {
        super(codigo,password, nombre, apellido, graduacion);
        this.codCuerpo = codCuerpo;
        this.codCompania = codCompania;
        this.codCuartel = codCuartel;
       // this.serviciosReailzados = serviciosRealizados;
    }
    */
    public SoldadoBean() {
    }

    public Integer getCodCuerpo() {
        return codCuerpo;
    }

    public void setCodCuerpo(Integer codCuerpo) {
        this.codCuerpo = codCuerpo;
    }

    public Integer getCodCompania() {
        return codCompania;
    }

    public void setCodCompania(Integer codCompania) {
        this.codCompania = codCompania;
    }

    public Integer getCodCuartel() {
        return codCuartel;
    }

    public void setCodCuartel(int codCuartel) {
        this.codCuartel = codCuartel;
    }

    @Override
    public String toString() {
        return "Soldado{" + "codCuerpo=" + codCuerpo + ", codCompania=" + codCompania + ", codCuartel=" + codCuartel + '}';
    }
 
}