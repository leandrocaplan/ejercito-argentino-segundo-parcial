package Beans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leandro
 */
public class CuerpoBean {
    private Integer codigo;
    private String denominacion;
    
  
    public CuerpoBean(Integer codigo, String denominacion) {
        this.codigo = codigo;
        this.denominacion = denominacion;
    }

    public CuerpoBean() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    @Override
    public String toString() {
        return "Cuerpo{" + "codigo=" + codigo + ", denominacion=" + denominacion + '}';
    }

}