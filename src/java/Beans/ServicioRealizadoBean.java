package Beans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Leandro
 */
public class ServicioRealizadoBean {

    private Integer codServicioRealizado;
    private Integer codSoldado;
    private Integer codServicio;
    private String fecha;

    public ServicioRealizadoBean(Integer codServicioRealizado, Integer codSoldado, Integer codServicio, String fecha) {
        this.codServicioRealizado = codServicioRealizado;
        this.codSoldado = codSoldado;
        this.codServicio = codServicio;
        this.fecha = fecha;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getCodServicioRealizado() {
        return codServicioRealizado;
    }

    public void setCodServicioRealizado(Integer codServicioRealizado) {
        this.codServicioRealizado = codServicioRealizado;
    }

    public Integer getCodSoldado() {
        return codSoldado;
    }

    public void setCodSoldado(Integer codSoldado) {
        this.codSoldado = codSoldado;
    }

    public Integer getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(Integer codServicio) {
        this.codServicio = codServicio;
    }

    @Override
    public String toString() {
        return "ServicioRealizado{" + "codServicioRealizado=" + codServicioRealizado + ", codSoldado=" + codSoldado + ", codServicio=" + codServicio + ", fecha=" + fecha + '}';
    }

}
