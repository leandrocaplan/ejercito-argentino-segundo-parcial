package Beans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Leandro
 */
public class CompaniaBean {
    private Integer codigo;
    private String actividad;

    public CompaniaBean(Integer codigo, String actividad) {
        this.codigo = codigo;
        this.actividad = actividad;
    }

    public CompaniaBean() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    @Override
    public String toString() {
        return "Compania{" + "codigo=" + codigo + ", actividad=" + actividad + '}';
    }
    
}
