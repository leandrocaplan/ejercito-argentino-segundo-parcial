package Beans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Set;

/**
 * Esta es una clase abstracta, que modeliza a un militar de cualquier tipo. Sus atributos son:
 * 1. Codigo
 * 2. Password
 * 3. Nombre
 * 4. Apellido
 * 5. Graduacion
 * Los mismos son comunes a cualquier tipo de militar (Oficial,Suboficial o Soldado)
 * @author Leandro
 */
public abstract class MilitarBean implements Serializable {
    
    private Integer codigo;
    private String tipo;
    private String password;
    
    private String nombre;
    private String apellido;
    private String graduacion;
    /**
     * Declaro un constructor que inicialize todos los parámetros
     * @param codigo
     * @param password
     * @param nombre
     * @param apellido
     * @param graduacion 
     */
    
    public MilitarBean(Integer codigo,String tipo,String password, String nombre, String apellido, String graduacion) {
        this.codigo = codigo;
        this.tipo=tipo;
        this.password=password;
        this.nombre = nombre;
        this.apellido = apellido;
        this.graduacion=graduacion;
    }
    
    /**
     * Declaro un constructor vacío
     */
    public MilitarBean() {
    }

    /**
     * Este método se utilizará para pedir al usuario una opcion por teclado y ejecutar la operacion correspondiente.
     * Este método es abstracto, ya que las clases hijas lo utilizaran de acuerdo al tipo de operaciones
     * que les corresponda realizar.
     * @param opcion
     * @param sistema 
     */    
   
    public String getTipo() {
        return tipo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public String getGraduacion() {
        return graduacion;
    }

    @Override
    public String toString() {
        return "Militar{" + "codigo=" + codigo + ", tipo=" + tipo + ", password=" + password + ", nombre=" + nombre + ", apellido=" + apellido + ", graduacion=" + graduacion + '}';
    }

   
    
    //Tengo que validar la graduacion.
   
    /**
     * Este método despliega un menú de opciones que me indicarán que operaciones puedo elegir de acuerdo al usuario
     * que lo esté utilizando.
     * Este método es abstracto, ya que cada clase hija desplegará un menú diferente de acuerdo a las operaciones
     * que realizará.
     */
    
}